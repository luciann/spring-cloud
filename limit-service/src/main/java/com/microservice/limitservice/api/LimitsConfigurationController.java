package com.microservice.limitservice.api;


import com.microservice.limitservice.entity.LimitConfiguration;
import com.microservice.limitservice.settings.Configuration;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/limits")
public class LimitsConfigurationController {

    @Autowired
    private Configuration configuration;

    @GetMapping
    public LimitConfiguration retrieveLimitsFromConfig() {
        return new LimitConfiguration(configuration.getMaximum(), configuration.getMinimum());
    }

    @GetMapping("/fault-tolerance-example")
    @HystrixCommand(fallbackMethod = "fallbackRetrieveConfiguration")
    public LimitConfiguration retrieveConfiguration() {
        throw new RuntimeException("");
    }

    public LimitConfiguration fallbackRetrieveConfiguration() {
        return new LimitConfiguration(2, 1);
    }
}
