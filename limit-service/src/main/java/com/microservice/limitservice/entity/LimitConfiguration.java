package com.microservice.limitservice.entity;

public class LimitConfiguration {

    public LimitConfiguration() {
    }

    public LimitConfiguration(int max, int min) {
        this.max = max;
        this.min = min;
    }

    private int max;

    private int min;

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }
}
