package com.microservice.currencyexchangeservice.service;


import com.microservice.currencyexchangeservice.entity.ExchangeValue;
import com.microservice.currencyexchangeservice.repository.ExchangeValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExchangeValueService {

    @Autowired
    private ExchangeValueRepository exchangeValueRepository;

    public ExchangeValue getExchangeValue(String from, String to) {
        return exchangeValueRepository.findByFromIgnoreCaseAndToIgnoreCase(from, to);
    }
}
