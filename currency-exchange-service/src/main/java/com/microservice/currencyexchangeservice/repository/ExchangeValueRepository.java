package com.microservice.currencyexchangeservice.repository;

import com.microservice.currencyexchangeservice.entity.ExchangeValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangeValueRepository extends JpaRepository<ExchangeValue, Integer> {
    ExchangeValue findByFromIgnoreCaseAndToIgnoreCase(String from, String to);
}
