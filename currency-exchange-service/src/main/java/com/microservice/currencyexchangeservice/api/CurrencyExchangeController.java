package com.microservice.currencyexchangeservice.api;

import com.microservice.currencyexchangeservice.entity.ExchangeValue;
import com.microservice.currencyexchangeservice.service.ExchangeValueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "/currency-exchange")
public class CurrencyExchangeController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ExchangeValueService exchangeValueService;

    @Autowired
    private Environment environment;

    @GetMapping("/from/{from}/to/{to}")
    public ExchangeValue retrieveExchangeValue(@PathVariable String from, @PathVariable String to) {
        Integer port = Integer.parseInt(environment.getProperty("server.port"));
        logger.info("exchanging from {} to {} ", from, to);
        return exchangeValueService.getExchangeValue(from, to).setPort(port);
    }
}
