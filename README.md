### Current App Details
# ports

	Limits Service	                    8080, 8081, ...
	Spring Cloud Config Server	        8888
	Currency Exchange Service	        8000, 8001, 8002, ..
	Currency Conversion Service	        8100, 8101, 8102, ...
	Netflix Eureka Naming Server	    8761
	Netflix Zuul API Gateway Server	    8765
	Zipkin Distributed Tracing Server	9411
	
	
	Links 
	http://localhost:15672/#/	-> RabbitMQ admin
	http://localhost:8080 		-> HAL-browser
	http://localhost:8761/		-> eureka
	


### Spring Cloud

	There are a wide variety of projects under the umbrella of spring cloud.

	Spring Cloud provides tools for developers to quickly build some of the common patterns in distributed systems
	(e.g. configuration management, service discovery, circuit breakers, intelligent routing, micro-proxy, control bus,
	one-time tokens, global locks, leadership election, distributed sessions, cluster state).


### Advantages

    * new technology and process adoption
        eg. each microservice can be written in a different language

    * dynamic scaling
        eg. during black friday you can scale up your apps

    * faster releases - new features


### Challenges

	I.Bounded context - how do you identify what the microservices should and shouldn't do?

		Empirical analysis -> it's an evolutionary process


	II. Configuration Management

		tools:
		* spring-cloud-config

	III. Dynamic scaling [creating new instances]

		eg. dynamic load balancing + distribute the load among the new instances

		tools:
		* Naming Server (Eureka)

			All instances of all microservices are registered by Eureka

		* Ribbon (Client Side Load Balancing)

		* Feign (Easier REST client)

	IV. Visibility

		Q: how do you identify where the bug is?
		A: centralized log

		 * Zipkin Distributed Tracing

		 * Netflix Zul API gateway

	V. Fault Tolerance

		tools:
		* Hystrix


### Centralized Microservice Configuration - Spring Cloud

	Spring Cloud Config provides server and client-side support for externalized configuration in a distributed system.
	With the Config Server you have a central place to manage external properties for applications across all environments.
	The concepts on both client and server map identically to the Spring Environment and PropertySource abstractions, so they fit very well with Spring applications,
	but can be used with any application running in any language.

	As an application moves through the deployment pipeline from dev to test and into production you can manage the configuration between those environments
	and be certain that applications have everything they need to run when they migrate.

	The default implementation of the server storage backend uses git so it easily supports labelled versions of configuration environments,
	as well as being accessible to a wide range of tooling for managing the content. It is easy to add alternative implementations and plug them in with Spring configuration.

	The app connecting to the spring-cloud-config service should rename its application.properties file into bootstrap.properties.


### Eureka

	Service Discovery: Eureka instances can be registered and clients can discover the instances using Spring-managed beans
	Service Discovery: an embedded Eureka server can be created with declarative Java configuration


### Spring Netflix Zuul

	Routing is an integral part of a microservice architecture. For example, / may be mapped to your web application, /api/users is mapped to the user service and /api/shop is mapped to the shop service.
	Zuul is a JVM-based router and server-side load balancer from Netflix.

	Netflix uses Zuul for the following:

		Authentication
		Insights
		Stress Testing
		Canary Testing
		Dynamic Routing
		Service Migration
		Load Shedding
		Security
		Static Response handling
		Active/Active traffic management


### Spring Cloud Sleuth / Zipkin [tracing requests]

	https://spring.io/blog/2016/02/15/distributed-tracing-with-spring-cloud-sleuth-and-spring-cloud-zipkin

	Spring Cloud Sleuth implements a distributed tracing solution for Spring Cloud, borrowing heavily from Dapper, Zipkin and HTrace.
	For most users Sleuth should be invisible, and all your interactions with external systems should be instrumented automatically.
	You can capture data simply in logs, or by sending it to a remote collector service.

	check google's https://ai.google/research/pubs/pub36356 / distributed tracing papers

	Zipkin
	https://zipkin.io/pages/quickstart

### RabbitMQ









### Spring-cloud-bus 
	
	https://spring.io/projects/spring-cloud-bus
	
	Spring Cloud Bus links nodes of a distributed system with a lightweight message broker. 
	This can then be used to broadcast state changes (e.g. configuration changes) or other management instructions. 
	The only implementation currently is with an AMQP broker as the transport, but the same basic feature set (and some more depending on the transport) is on the roadmap for other transports.













