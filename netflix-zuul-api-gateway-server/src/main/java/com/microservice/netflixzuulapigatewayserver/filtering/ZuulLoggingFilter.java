package com.microservice.netflixzuulapigatewayserver.filtering;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

@Component
public class ZuulLoggingFilter extends ZuulFilter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    //before the request, after, only error etc. ?
    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    //priority of the filter
    @Override
    public int filterOrder() {
        return 1;
    }

    //should this filter be executed or not?
    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
        logger.info("Request ->>> {} request uri ->> {}  ", request, request.getRequestURI());
        return null;
    }
}
